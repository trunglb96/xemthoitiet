package com.thuyca.XemThoiTiet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomListAdapter extends BaseAdapter {
    private List<Icon> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public CustomListAdapter(Context aContext,  List<Icon> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.list_item_layout, null);
            holder = new ViewHolder();
            holder.flagView = (ImageView) view.findViewById(R.id.imageView_flag);
            holder.titleView = (TextView) view.findViewById(R.id.textView_Title);
            holder.descriptView = (TextView) view.findViewById(R.id.textView_Descript);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Icon icon = this.listData.get(i);
        holder.titleView.setText("Chữ hoa: " + icon.getTitleIcon());
        holder.descriptView.setText("Chữ thường: " + icon.getDescriptIcon());

        int imageId = this.getMipmapResIdByName(icon.getImageIcon());

        holder.flagView.setImageResource(imageId);

        return view;
    }

    public int getMipmapResIdByName(String resName)  {
        String pkgName = context.getPackageName();
        int resID = context.getResources().getIdentifier(resName , "mipmap", pkgName);
        return resID;
    }

    static class ViewHolder {
        ImageView flagView;
        TextView titleView;
        TextView descriptView;
    }
}
