package com.thuyca.XemThoiTiet;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.thuyca.XemThoiTiet.databinding.FragmentThirdBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLOutput;
import java.text.DecimalFormat;

public class ThirdFragment extends Fragment {

    private FragmentThirdBinding binding;
    private MediaPlayer mPlayer;
    private DecimalFormat df = new DecimalFormat("#.##");

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentThirdBinding.inflate(inflater, container, false);

        Bundle bundle = this.getArguments();
        String result = "";
        if (bundle != null) {
            String detail = bundle.getString("detail");
            try {
                JSONObject jsonObject = new JSONObject(detail);
                JSONArray jsonArray = jsonObject.getJSONArray("weather");
                JSONObject jsonObjectWeather = jsonArray.getJSONObject(0);
                String description = jsonObjectWeather.getString("description");
                JSONObject jsonObjectMain = jsonObject.getJSONObject("main");
                double temp = jsonObjectMain.getDouble("temp") - 273.15;
                double feelsLike = jsonObjectMain.getDouble("feels_like") - 273.15;
                float pressure = jsonObjectMain.getInt("pressure");
                int humidity = jsonObjectMain.getInt("humidity");
                JSONObject jsonObjectWind = jsonObject.getJSONObject("wind");
                String wind = jsonObjectWind.getString("speed");
                JSONObject jsonObjectClouds = jsonObject.getJSONObject("clouds");
                String clouds = jsonObjectClouds.getString("all");
                JSONObject jsonObjectSys = jsonObject.getJSONObject("sys");
                String countryname = jsonObjectSys.getString("country");
                String cityname = jsonObject.getString("name");

                result = "Current weather of " + cityname + " (" + countryname + ")"
                        + "\n Temp: " + df.format(temp) + " *C"
                        + "\n Feels Like: " + df.format(feelsLike) + " *C"
                        + "\n Humidity: " + humidity + "%"
                        + "\n Description: " + description
                        + "\n Wind Speed: " + wind + "m/s (meters per second)"
                        + "\n Cloudiness: " + clouds + "%"
                        + "\n Pressure: " + pressure + " hPa";
            } catch (JSONException e) {
                e.printStackTrace();
            }
            binding.titleThird.setText(result);
//            Toast.makeText(getContext(), result, Toast.LENGTH_LONG).show();
        }


        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ThirdFragment.this)
                        .navigate(R.id.action_ThirdFragment_to_FirstFragment);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public int getMipmapResIdByName(String resName)  {
        String pkgName = getContext().getPackageName();
        int resID = getContext().getResources().getIdentifier(resName , "mipmap", pkgName);
        return resID;
    }
}