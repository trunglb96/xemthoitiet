package com.thuyca.XemThoiTiet;

import java.io.Serializable;

public class Icon implements Serializable {
    private String imageIcon;
    private int soundIcon;
    private String titleIcon;
    private String descriptIcon;

    public Icon() {
    }

    public Icon(String imageIcon, int soundIcon, String titleIcon, String descriptIcon) {
        this.imageIcon = imageIcon;
        this.soundIcon = soundIcon;
        this.titleIcon = titleIcon;
        this.descriptIcon = descriptIcon;
    }

    public String getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(String imageIcon) {
        this.imageIcon = imageIcon;
    }

    public int getSoundIcon() {
        return soundIcon;
    }

    public void setSoundIcon(int soundIcon) {
        this.soundIcon = soundIcon;
    }

    public String getTitleIcon() {
        return titleIcon;
    }

    public void setTitleIcon(String titleIcon) {
        this.titleIcon = titleIcon;
    }

    public String getDescriptIcon() {
        return descriptIcon;
    }

    public void setDescriptIcon(String descriptIcon) {
        this.descriptIcon = descriptIcon;
    }

    @Override
    public String toString() {
        return "Icon{" +
                "imageIcon='" + imageIcon + '\'' +
                ", soundIcon='" + soundIcon + '\'' +
                ", titleIcon='" + titleIcon + '\'' +
                ", descriptIcon='" + descriptIcon + '\'' +
                '}';
    }
}
