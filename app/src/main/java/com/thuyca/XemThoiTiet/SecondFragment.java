package com.thuyca.XemThoiTiet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.fragment.NavHostFragment;

import com.thuyca.XemThoiTiet.databinding.FragmentSecondBinding;

import java.util.ArrayList;
import java.util.List;

public class SecondFragment extends Fragment {

    private FragmentSecondBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentSecondBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.buttonSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        List<Icon> image_details = getListData();
        ListView listView = (ListView) view.findViewById(R.id.listView_second);
        listView.setAdapter(new CustomListAdapter(getContext(), image_details));

        // When the user clicks on the ListItem
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = listView.getItemAtPosition(position);
                Icon icon = (Icon) o;
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_ThirdFragment);

                ThirdFragment fragment = new ThirdFragment();
                Bundle bundle = new Bundle();
                bundle.putString("imageIcon", icon.getImageIcon());
                bundle.putInt("soundIcon", icon.getSoundIcon());
                bundle.putString("titleIcon", icon.getTitleIcon());
                bundle.putString("descriptIcon", icon.getDescriptIcon());

                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.nav_host_fragment_content_main , fragment);

                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private  List<Icon> getListData() {
        List<Icon> list = new ArrayList<Icon>();
        list.add(new Icon("icon1", R.raw.sound1, "A", "a"));
        list.add(new Icon("icon2", R.raw.sound2, "Ă", "ă"));
        list.add(new Icon("icon3", R.raw.sound3, "Â", "â"));
        list.add(new Icon("icon4", R.raw.sound4, "B", "b"));
        list.add(new Icon("icon5", R.raw.sound5, "C", "c"));
        list.add(new Icon("icon6", R.raw.sound6, "D", "d"));
        list.add(new Icon("icon7", R.raw.sound7, "Đ", "đ"));
        list.add(new Icon("icon8", R.raw.sound8, "E", "e"));
        list.add(new Icon("icon9", R.raw.sound9, "Ê", "e"));
        list.add(new Icon("icon10", R.raw.sound10, "G", "g"));
        list.add(new Icon("icon11", R.raw.sound11, "H", "h"));
        list.add(new Icon("icon12", R.raw.sound12, "I", "i"));
        list.add(new Icon("icon13", R.raw.sound13, "K", "k"));
        list.add(new Icon("icon14", R.raw.sound14, "L", "l"));
        list.add(new Icon("icon15", R.raw.sound15, "M", "m"));
        list.add(new Icon("icon16", R.raw.sound16, "N", "n"));
        list.add(new Icon("icon17", R.raw.sound17, "O", "o"));
        list.add(new Icon("icon18", R.raw.sound18, "Ô", "ô"));
        list.add(new Icon("icon19", R.raw.sound19, "Ơ", "ơ"));
        list.add(new Icon("icon20", R.raw.sound20, "P", "p"));
        list.add(new Icon("icon21", R.raw.sound21, "Q", "q"));
        list.add(new Icon("icon22", R.raw.sound22, "R", "r"));
        list.add(new Icon("icon23", R.raw.sound23, "S", "s"));
        list.add(new Icon("icon24", R.raw.sound24, "T", "t"));
        list.add(new Icon("icon25", R.raw.sound25, "U", "u"));
        list.add(new Icon("icon26", R.raw.sound26, "Ư", "ư"));
        list.add(new Icon("icon27", R.raw.sound27, "V", "v"));
        list.add(new Icon("icon28", R.raw.sound28, "X", "x"));
        list.add(new Icon("icon29", R.raw.sound29, "Y", "y"));

        return list;
    }

}